import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tax-information',
  templateUrl: './tax-information.component.html',
  styleUrls: ['./tax-information.component.scss']
})
export class TaxInformationComponent implements OnInit {

   ELEMENT_DATA = [
    {Taxeringsar: 'Taxerad förvärvsinkomst', savings: '148 400', debt: '108 300'},
    {Taxeringsar: 'Inkomst av tjänst', savings: '148 411', debt: '108 384'},
    {Taxeringsar: 'Överskott av kapital', savings: '0', debt: '0'},
    {Taxeringsar: 'Underskott av kapital', savings: '5347', debt: '6207'},
    {Taxeringsar: 'Fastighetsskatt', savings: '0', debt: '0'},
    {Taxeringsar: 'Moms', savings: '0', debt: '0'},
    {Taxeringsar: 'Slutlig skatt', savings: '47353', debt: '25 199'}
   ];

  displayedColumns: string[] = ['Taxeringsar', 'savings', 'debt'];
  dataSource = this.ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
