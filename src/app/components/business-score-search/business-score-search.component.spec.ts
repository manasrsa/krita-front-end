import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessScoreSearchComponent } from './business-score-search.component';

describe('BusinessScoreSearchComponent', () => {
  let component: BusinessScoreSearchComponent;
  let fixture: ComponentFixture<BusinessScoreSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessScoreSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessScoreSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
