import { Component, EventEmitter, Output } from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import { HttpService } from 'src/app/services/httpService/http.service';

@Component({
  selector: 'app-business-score-search',
  templateUrl: './business-score-search.component.html',
  styleUrls: ['./business-score-search.component.scss']
})
export class BusinessScoreSearchComponent {
  organizationForm: FormGroup;
  orgNo!: string;
  basicInfoData:any = [];
  showLoader = false;
  @Output() responseData = new EventEmitter<any>();
  orgFormErr: boolean = false;

  constructor(private fb : FormBuilder, public httpService: HttpService) {
    this.organizationForm =  this.fb.group({
      orgNo : ["", Validators.required]
    });
  }


  onSearch() {
    let formData = this.organizationForm.controls;
    if(formData.orgNo.value === '1111111') {
      this.getBasicInfo();
      this.orgFormErr = false;
    } else {
      this.organizationForm.controls.orgNo.setErrors({required:true});
    }
  }

  getBasicInfo() {
    this.showLoader = true;
    const payload = {
      mallkod: "2FGS",
      belopp: "2000",
      loptid: "6",
      userid: "AH87XM3",
      password: "Sommar88"
    }
    let dataObj =
    {
      url: "https://kritawebapp3.azurewebsites.net/FtgScoring",
      method: "POST",
      requestBodyData: payload
    };
    this.httpService.callApi(dataObj).subscribe(
      response => {
        this.showLoader = false;
        this.responseData.emit(response);
        
      },
      error => {
        this.showLoader = false;
        console.log("error in get items", error);
      }
    );
  }

}
