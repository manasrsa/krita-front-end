import { Component, Input, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/httpService/http.service';
@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent {

  @Input() basicInfoData: any;

}
