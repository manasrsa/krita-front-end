import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenueInfoComponent } from './revenue-info.component';

describe('RevenueInfoComponent', () => {
  let component: RevenueInfoComponent;
  let fixture: ComponentFixture<RevenueInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RevenueInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RevenueInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
