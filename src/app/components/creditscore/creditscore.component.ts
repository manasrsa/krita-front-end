import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-creditscore',
  templateUrl: './creditscore.component.html',
  styleUrls: ['./creditscore.component.scss']
})
export class CreditscoreComponent implements OnInit {
  public canvasWidth = 600
  public needleValue = 75
  public centralLabel = 'Credit Score:'
  public bottomLabel = '2'
  public bottomLabelFont = 5
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    rangeLabelFontSize: 10,
    arcColors: ['rgb(228, 74, 0)', 'rgb(248, 189, 25)', 'rgb(107, 170, 1)'],
    arcDelimiters: [15, 60],
    rangeLabel: ['', ''],
    needleStartValue: 1,
}

  constructor() { }

  ngOnInit(): void {

  }

}
