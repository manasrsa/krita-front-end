import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtinfoComponent } from './debtinfo.component';

describe('DebtinfoComponent', () => {
  let component: DebtinfoComponent;
  let fixture: ComponentFixture<DebtinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebtinfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
