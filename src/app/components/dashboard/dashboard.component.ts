import { Component, OnInit } from '@angular/core';
import { ApplicationUser } from '../../services/application/applicationUser';
import { LoaderService } from '../../shared/loader/service/loader.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastsComponent } from "../../shared/toasts/toasts.component"
import { ToastsService } from '../../shared/toasts/service/toast-service';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userName: string = '';
  allApplications: any = [];
  basicInfoData: any = [];
  constructor(private applicationUser: ApplicationUser,
    private loadService: LoaderService,
    private toastservice: ToastsService,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private cookieService: CookieService,
    private languageService: LanguageService) {
      this.languageService.getLanguage().subscribe((res:any) => {
        console.log(res.language);
        this.translate.use(res.language)
      })
     }

  ngOnInit() {
    // //get user name
    // this.applicationUser.getUserFullName().subscribe((res: any) => {

    //   if (res.result.resultCode == 0) {
    //     try {
    //       this.userName = res.data;
    //     }
    //     catch (err:any) {
    //       this.toastMessage({
    //         msg: ['data langeuage  ' + err.messaage],
    //         type: 'error'
    //       });
    //     }

    //   }
    //   else {
    //     this.toastMessage({
    //       msg: [res.result.resultText],
    //       type: 'error'
    //     })
    //   }
    // }, (error: any) => {

    //   this.toastMessage({
    //     msg: ['Status : ' + error.status + '  Response :' + error.statusText],
    //     type: "error"
    //   })
    // }, () => { })
    // // this.getAllApplication();
    // this.setLocalisation();
  }
  //set localization 
  setLocalisation() {
    this.translate.addLangs(['sv', 'en', 'fi']);
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      if (lang) {
        let langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.translate.use(langCode);
      }
      else {
        this.translate.use('sv');
      }
    }
  }
  //get all application
  getAllApplication() {

    this.applicationUser.getApplication().subscribe((res: any) => {
      if (res.result.resultCode == 0) {
        this.loadService.stopLoading();
        try {
          this.allApplications = res.data;
        }
        catch (err:any) {
          this.toastMessage({
            msg: ['data langeuage  ' + err.messaage],
            type: 'error'
          });
        }


      }
      else {
        this.toastMessage({
          msg: [res.result.resultText],
          type: 'error'
        });
        this.loadService.stopLoading();
      }
    }, (error:any) => {

      this.loadService.stopLoading();
      this.toastMessage({
        msg: ['Status : ' + error.status + '  Response :' + error.statusText],
        type: "error"
      })

    }, () => { })
  }
  //error message service call
  toastMessage(message: any) {

    this.toastservice.errorSnackBar(message);
    this.snackBar.openFromComponent(ToastsComponent
      , {
        panelClass: message.type,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
  }


  onResponseData(event:any) {
    this.basicInfoData = event;
  }

}
