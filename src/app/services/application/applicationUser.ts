/*********************************************************************
Name: ApplicationUser
=================================================================
Purpose:Create service for LIST,UPDATE,SEARCH 

==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Mukesh Kumar singh         08/05/2019       Initial Version

****************************************************************/

import { Injectable } from "@angular/core";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/Rx';
import { map } from 'rxjs/operators';
import { urlConfigs } from "../url.config";
import { CustomHttpClient } from "../../shared/services/httpClient/http-client.service";
import { LoaderService } from "../../shared/loader/service/loader.service";

@Injectable({
  providedIn: 'root',
})
export class ApplicationUser {
  API_URL: any = '';
  constructor(
    private http: CustomHttpClient, 
    private _global: urlConfigs,
    ) {
      this.API_URL = this._global.baseAppUrl + 'user/';
  }
  //Get user full name 
  getUserFullName(){
    return  this.http.get(this.API_URL + 'fullName').pipe(map((res:any)=>{
      return res;
    }));
   
   }
   //Get App application        
   getApplication() {
     return this.http.get(this.API_URL + 'allowedapplications').pipe(map((res:any)=>{
       return res;
     }));
   }
}
