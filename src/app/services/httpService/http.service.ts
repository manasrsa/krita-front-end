import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(public http: HttpClient) { }

  callApi(dataObject: any) : Observable <any> {
    let method = dataObject.method;
    let url = dataObject.url;
    let headers : any = new HttpHeaders({'content-Type' : 'application/json'});

    let option = {
      body : dataObject.requestBodyData || {},
      headers : headers
    };

    return this.http.request(method, url, option)
      .pipe(
        tap(
          (result: any) => {
            return result;
          },
          (error: any) => {
            console.log("fetched error", error);
            return error;
          }
        )
      );
  }
}