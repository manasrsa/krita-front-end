import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import{ Observable } from "rxjs/Observable";
import { urlConfigs } from "./url.config";

import { CustomHttpClient } from "../shared/services/httpClient/http-client.service";

@Injectable({
    providedIn: 'root'
  })
export class LanguageService{
    private subject = new Subject<any>();
    API_URL: any = '';
    constructor(private httpClient: CustomHttpClient, 
            private _global: urlConfigs) {
        this.API_URL = this._global.baseAppUrl + 'Utility';
    }
    setLanguage(lng:string)
    {
        this.subject.next({"language": lng });
      
    }
    getLanguage(): Observable<any> {
        return this.subject.asObservable();
    }
    setLanguageCookies(lng:string)
    {
        return this.httpClient.get(this.API_URL + '/SetCulture?culture=' + lng);
        
    }
    ///Utilities/SetCookies?culture=fi-FI
}
