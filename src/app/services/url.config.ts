/*********************************************************************
Name: urlConfigs
=================================================================
Purpose:Declare the url

==================================================================
History
--------
Version   Author                     Date              Detail
1.0       Mukesh Kumar singh         08/05/2019       Initial Version

****************************************************************/ 

import { Injectable } from '@angular/core';

@Injectable({
    providedIn:'root'
})
export class urlConfigs {
    readonly baseAppUrl: string = '/api/';
    //readonly baseAPIUrl: string = 'https://api.github.com';
}