import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(public translate: TranslateService,
    private cookieService: CookieService) {

  }
  ngOnInit() {
    this.translate.addLangs(['sv', 'en', 'fi'])
    if (this.translate.getBrowserLang() !== undefined) {
      let lang = this.cookieService.get('.AspNetCore.Culture');
      if (lang) {
        var langCode = lang.split("|")[0].split("=")[1].split("-")[0];
        this.translate.use(langCode);
      }
    }
    else {
      this.translate.use('sv');
    }
  }
}
