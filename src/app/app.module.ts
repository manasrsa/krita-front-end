import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BasicInfoComponent } from './components/basic-info/basic-info.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TabmenuComponent } from './components/tabmenu/tabmenu.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslateLoader, TranslateModule, TranslateStore  } from "@ngx-translate/core";
import { CreditscoreComponent } from './components/creditscore/creditscore.component';
import { BoardInfoComponent } from './components/board-info/board-info.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { GaugeChartModule } from 'angular-gauge-chart';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BusinessScoreSearchComponent } from './components/business-score-search/business-score-search.component';
import { RevenueInfoComponent } from './components/revenue-info/revenue-info.component';
import { DebtinfoComponent } from './components/debtinfo/debtinfo.component';
import { TaxInformationComponent } from './components/tax-information/tax-information.component';
import { MatTableModule } from '@angular/material/table';
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { ToastsComponent } from './shared/toasts/toasts.component';
import { LoaderComponent } from './shared/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    BasicInfoComponent,
    DashboardComponent,
    TabmenuComponent,
    ToastsComponent,
    SidenavComponent,
    LoaderComponent,
    CreditscoreComponent,
    BoardInfoComponent,
    BusinessScoreSearchComponent,
    RevenueInfoComponent,
    DebtinfoComponent,
    TaxInformationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    GaugeChartModule,
    MatSnackBarModule,
    AppRoutingModule,
    MatTableModule,
    MatTabsModule,
    NgxSkeletonLoaderModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      },
    })
  ],
  providers: [TranslateStore],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "./assets/i18n/home/", ".json");
}
