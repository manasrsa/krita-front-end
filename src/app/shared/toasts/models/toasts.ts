export class Toasts {
    type!: ToastsType;
    message: string | undefined;
    alertId!: string;
    keepAfterRouteChange!: boolean;

    constructor(init?:Partial<Toasts>) {
        Object.assign(this, init);
    }
}

export enum ToastsType {
    Success,
    Error,
    Info,
    Warning
}
