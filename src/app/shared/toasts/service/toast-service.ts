import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Toasts } from '../models/toasts';
@Injectable({
    providedIn: 'root'
  })
  
export class ToastsService {
    durationInSeconds = 5;
    snackbarMsg:any='';

    constructor(private snackBar: MatSnackBar){}

    private subject = new Subject<Toasts>();
  errorSnackBar(msg:any) {
   
        this.snackbarMsg = msg;
  }
    closeToast(){
      this.snackbarMsg={ msg: [""], type: 'error' };
        this.snackBar.dismiss();
    }
}
