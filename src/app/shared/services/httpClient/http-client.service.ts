import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Injectable({
    providedIn:'root'
})
export class CustomHttpClient {
  http: HttpClient;
  urlPrefix!: string;
  // Http Options
   
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Basic ` + btoa('user:password'),
    })

    }



  constructor(http: HttpClient) {
    this.http = http;
  }
 
  get(url: string) {
    return this.http.get(url).pipe(catchError((err)=>{return Observable.throw(err)}));
  }
  // getById(url,id){
  //     return this.http.get(this.urlPrefix+url,id);
  // }

  post(url: string, data: any) {
    return this.http.post(url, data).pipe(catchError((err)=>{return Observable.throw(err)}));
  }
  put(url: string,data: any)
  {
    return this.http.put(this.urlPrefix + url,data).pipe(catchError((err)=>{return Observable.throw(err)}));
  }
  request(url: string,formData: any)
  {
    const uploadReq = new HttpRequest('POST',url , formData, {
      reportProgress: true,
    });
    return this.http.request(uploadReq).pipe(catchError((err)=>{return Observable.throw(err)}));
  }
}
